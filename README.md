##In this Exercise you're going to use CSS combinators, grouping and specificity to style some elements

##Part 1

###1. Download provided HTML Template
###2. Make the [li] decedents GREEN. (Be mindful of the order of our cascading stylesheet)
###3. Then make ALL immediate [li] children of the parent RED
##Only the nested list items should be green now
###4. At the bottom of the HTML page, there is a {div} with a class="parent". Using child and descendent selectors, make the first and second [h2] tags two different colors of your choice. (Use the Specificity point system).

##Part 2

###1. Using ONE Css style rule, apply the same text color to ALL [h1], [h3], [h4] and the [p] Class
###2. Using SPECIFICITY with the [h3] tag, create another style rule and change the color of the [h3] text.
